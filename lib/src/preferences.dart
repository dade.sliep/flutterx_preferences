import 'dart:async';
import 'dart:convert';

import 'package:flutterx_preferences/src/impl/local_storage.dart'
    if (dart.library.html) 'package:flutterx_preferences/src/impl/local_storage_html.dart';
import 'package:flutterx_preferences/src/impl/session_storage.dart'
    if (dart.library.html) 'package:flutterx_preferences/src/impl/session_storage_html.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

/// A map that manages to store and persist data within it's scope.
/// See [sessionStorage] and [localStorage]
typedef Storage = Map<String, String>;

class Preferences {
  final Storage data;

  Preferences(this.data);

  Preferences.local([String name = '_local_']) : this(loadLocalStorage(name));

  Preferences.session() : this(sessionStorage);

  static Future<void> initialize() => initializeLocalStorage();

  Set<String> getKeys() => data.keys.toSet();

  bool containsKey(String key) => data.containsKey(key);

  T get<T>(String key, {required T Function(String source) decode, required T fallback}) =>
      opt<T>(key, decode: decode) ?? fallback;

  T getJson<T>(String key, {required FromJsonObject<T> fromJson, required T fallback}) =>
      optJson(key, fromJson: fromJson) ?? fallback;

  bool getBool(String key, {bool fallback = false}) => optBool(key) ?? fallback;

  int getInt(String key, {int fallback = 0}) => optInt(key) ?? fallback;

  double getDouble(String key, {double fallback = 0}) => optDouble(key) ?? fallback;

  String getString(String key, {String fallback = ''}) => optString(key) ?? fallback;

  List<T> getList<T>(String key, {required T Function(String source) decode, List<T> fallback = const []}) =>
      optList(key, decode: decode) ?? fallback;

  T? opt<T>(String key, {required T Function(String source) decode}) => _opt<T>(key, decode);

  T? optJson<T>(String key, {required FromJsonObject<T> fromJson}) =>
      opt(key, decode: (source) => fromJson(jsonDecode(source)));

  bool? optBool(String key) => _opt(key, (source) => source == 'true');

  int? optInt(String key) => _opt(key, int.tryParse);

  double? optDouble(String key) => _opt(key, double.tryParse);

  String? optString(String key) => _opt(key);

  List<T>? optList<T>(String key, {required T Function(String source) decode}) {
    final value = data[key];
    if (value == null) return null;
    final list = jsonDecode(value) as List;
    return list.map<T>((item) => decode(item)).toList(growable: false);
  }

  void set<T>(String key, T? value, {String Function(T value) encode = jsonEncode}) =>
      _set(key, value == null ? null : encode(value));

  void setBool(String key, bool? value) => _set(key, value?.toString()); // ignore: avoid_positional_boolean_parameters

  void setInt(String key, int? value) => _set(key, value?.toString());

  void setDouble(String key, double? value) => _set(key, value?.toString());

  void setString(String key, String? value) => _set(key, value);

  void setList<T>(String key, List<T>? value, {String Function(T value) encode = jsonEncode}) =>
      set<List<T>>(key, value, encode: (value) => jsonEncode(value.map(encode).toList(growable: false)));

  void remove(String key) => data.remove(key);

  void clear() => data.clear();

  T? _opt<T>(String key, [T? Function(String value)? decode]) {
    final value = data[key];
    return value == null
        ? null
        : decode == null
            ? value as T
            : decode(value);
  }

  void _set(String key, String? value) => value == null ? data.remove(key) : data[key] = value;
}
