import 'dart:collection';
import 'dart:convert';
import 'dart:html' as html; // ignore: avoid_web_libraries_in_flutter

import 'package:flutter/foundation.dart';
import 'package:flutterx_preferences/src/preferences.dart';

Storage get localStorage => html.window.localStorage;

Storage loadLocalStorage(String name) => _LocalStorage.fromName(name);

Future<void> initializeLocalStorage() => SynchronousFuture(null);

class _LocalStorage with MapMixin<String, String> {
  final String _name;
  final Map<String, dynamic> _data;

  _LocalStorage._(this._name, this._data);

  factory _LocalStorage.fromName(String name) {
    final content = html.window.localStorage[name] ?? '';
    final data = content.isEmpty ? const <String, dynamic>{} : jsonDecode(content);
    return _LocalStorage._(name, Map.of(data));
  }

  @override
  String? operator [](Object? key) => _data[key];

  @override
  void operator []=(String key, String value) {
    if (_data[key] != value) {
      _data[key] = value;
      _sync();
    }
  }

  @override
  void clear() {
    if (_data.isNotEmpty) {
      _data.clear();
      _sync();
    }
  }

  @override
  Iterable<String> get keys => _data.keys;

  @override
  String? remove(Object? key) {
    final length = _data.length;
    final result = _data.remove(key);
    if (_data.length != length) _sync();
    return result;
  }

  void _sync() => html.window.localStorage[_name] = jsonEncode(_data);
}
