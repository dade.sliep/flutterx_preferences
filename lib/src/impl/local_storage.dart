import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutterx_preferences/src/preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:path_provider/path_provider.dart';

Storage localStorage = loadLocalStorage('_local_');

Storage loadLocalStorage(String name) => _LocalStorage.fromName(name);

Directory? _dataDir;

Future<void> initializeLocalStorage() async => _dataDir ??= await getApplicationSupportDirectory();

class _LocalStorage with MapMixin<String, String> {
  final File _file;
  final Map<String, dynamic> _data;

  _LocalStorage._(this._file, this._data);

  factory _LocalStorage.fromName(String name) {
    final directory =
        requireNotNull(_dataDir, message: () => 'dataDir cannot be found. Have you called Preferences.initialize()?');
    final file = File('${directory.path}/$name.local');
    var data = const <String, dynamic>{};
    if (!file.existsSync()) {
      file.createSync();
    } else {
      final content = file.readAsStringSync();
      if (content.isNotEmpty) data = jsonDecode(content);
    }
    return _LocalStorage._(file, Map.of(data));
  }

  @override
  String? operator [](Object? key) => _data[key];

  @override
  void operator []=(String key, String value) {
    if (_data[key] != value) {
      _data[key] = value;
      _sync();
    }
  }

  @override
  void clear() {
    if (_data.isNotEmpty) {
      _data.clear();
      _sync();
    }
  }

  @override
  Iterable<String> get keys => _data.keys;

  @override
  String? remove(Object? key) {
    final length = _data.length;
    final result = _data.remove(key);
    if (_data.length != length) _sync();
    return result;
  }

  void _sync() => _file.writeAsStringSync(jsonEncode(_data));
}
