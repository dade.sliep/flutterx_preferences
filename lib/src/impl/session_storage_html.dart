import 'dart:html' as html; // ignore: avoid_web_libraries_in_flutter

import 'package:flutterx_preferences/src/preferences.dart';

Storage get sessionStorage => html.window.sessionStorage;
