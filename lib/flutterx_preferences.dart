library flutterx_preferences;

export 'package:flutterx_preferences/src/impl/local_storage.dart'
    if (dart.library.html) 'package:flutterx_preferences/src/impl/local_storage_html.dart' hide initializeLocalStorage;
export 'package:flutterx_preferences/src/impl/session_storage.dart'
    if (dart.library.html) 'package:flutterx_preferences/src/impl/session_storage_html.dart';
export 'package:flutterx_preferences/src/preferences.dart';
