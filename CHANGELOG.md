## 1.2.5

* Remove LiveData dependency

## 1.2.4

* Refactoring, add local storage map support

## 1.2.3

* Fix web error

## 1.2.2

* Refactoring

## 1.2.1

* Add json methods

## 1.2.0

* Improvements

## 1.1.1-dev

* Improvements

## 1.1.0

* Initial stable release

## 1.0.5-dev

* Platform management improvements.

## 1.0.4-dev

* Add web support.

## 1.0.3-dev

* Fix dispatcher for LivePreferences.

## 1.0.2-dev

* Make everything synchronous.

## 1.0.1-dev

* Fix.

## 1.0.0-dev

* Initial release.
