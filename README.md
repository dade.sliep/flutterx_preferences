# flutterx_preferences

Persist and query shared data in a fast and reliable way, integrate LiveData to observe any change to active preferences

## Import

Import the library like this:

```dart
import 'package:flutterx_preferences/flutterx_preferences.dart';
```

## Usage

Check the documentation in the desired source file of this library