import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class AppSettings extends SelfLiveData<AppSettings> {
  static final AppSettings instance = AppSettings._();
  final LiveMap<String, String> _storage = LiveMap(source: loadLocalStorage('app_settings'));

  AppSettings._() {
    setFields({_storage});
  }

  Color get appColor => Color(_storage.getInt('appColor', fallback: Colors.blue.value));

  set appColor(Color value) => _storage['appColor'] = value.value.toString();
}
