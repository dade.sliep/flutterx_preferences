import 'package:example/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';

Future<void> main() async {
  await Preferences.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => LiveDataBuilder<AppSettings>(
        data: AppSettings.instance,
        builder: (context, settings) => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutterx Preferences Demo',
            theme: ThemeData.from(
              colorScheme: generateScheme(
                primary: settings.appColor,
                onLight: Colors.grey.shade600,
                onDark: Colors.grey.shade400,
              ),
            ),
            home: const PreferencesExample()),
      );
}

class PreferencesExample extends StatefulWidget {
  const PreferencesExample({Key? key}) : super(key: key);

  @override
  State<PreferencesExample> createState() => _PreferencesExampleState();
}

class _PreferencesExampleState extends State<PreferencesExample> {
  final List<Color> _colors = Colors.primaries;
  final Preferences _preferences = Preferences.local('preferences');

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('Preferences example'), backgroundColor: Theme.of(context).primaryColor),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: ToggleButtons(
                isSelected: _colors.map((color) => AppSettings.instance.appColor == color).toList(),
                onPressed: (value) => AppSettings.instance.appColor = _colors[value],
                renderBorder: false,
                children: _colors
                    .map((color) => Material(
                        shape: const CircleBorder(),
                        elevation: 2,
                        color: color,
                        child: SizedBox.fromSize(size: const Size.square(40))))
                    .toList()),
          ),
          const SizedBox(height: 100),
          Text('pressed ${_preferences.getInt('counter')} times'),
        ]),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            final counter = _preferences.getInt('counter');
            _preferences.setInt('counter', counter + 1);
            setState(() {});
          },
          child: const Icon(Icons.add)));
}
